finit
=====

## TODO: Update this doc with current status and changes

finit is the Figbox Init & Services system. The name finit is obviously a bit of a joke, as well as standing for "FIG init"... as "finit" means ends, and initialise means to start...

It is essentially a combination of two different types of init: classic research UNIX rc init with a couple of BSD rc init extensions, and more structured services based on /etc/inittab with extensions.

If finit is called as PID 1 with the basename 'init' then it will act as the actual system rc init program.

If it is called with the basename 'finit' then it will act as the finit extended service controller/launcher, regardless of PID.

If called with basename finitctl then it will act as the user control program for finit services. finit and finitctl will not work unless compiled to do structured finit services.

The program can be recompiled to only include finit services, or rc init, or both depending on requirements.

By default the finit program only loads services from /etc/finit.d, however you can instead pass a PATH-like argument (directories delimited by ':'), or single directory to tell it to look somewhere else.

The last argument is assumed to be the service runlevel to go up to on launch.

finit services is partially compatible with /etc/inittab formatted service files if you want to use them without extensions, but the file MUST have the basename inittab.


RC Init
-------

See https://github.com/v7unix/v7unix/blob/master/v7/usr/src/cmd/init.c for basic structure.

Init will run /etc/rc, followed by /etc/rc.local, followed by scripts in /etc/rc.d all done consecutively in whatever order it finds them, then once complete will launch getty on terminals based on the settings in /etc/ttys.

When entering a pseudo runlevel, any children of the script /etc/rc.{old level} are killed, and the script /etc/rc.{new level} is run.

Scripts in  /etc/local.d, /etc/init.d and /etc/rc{runlevel}.d directories are only convention and should be run via other init scripts entry specifying how and when to do so.

### Runlevels

As an extension the RC init part of finit allows for pseudo-runlevels.

It is assumed that if no argument, or by default, the init will launch as runlevel 3.

0 = Off, if entering this level from a higher level, then this will shutdown the system. If the system starts up and only reaches level 0, it will halt for further input without any initialisation.

1 = Single-user / System Control. This only activates a single tty, launching a simple shell locked onto the /dev/console device. Does not run rc scripts, start any services, or mount any drives (root already mounted by kernel). 

2 - 5 = Multiuser with rc scripts, services, mounts, and gettys

6 = Restart: set a volatile flag to tell init to automatically come back to the same init level as before entering 6 upon reaching 0, then reset the system.

For compatbility, the finit rc init uses the fifo file /run/initctl to receive changes to runlevel; changing
runlevels are done by monitoring the fifo for ascii encoded 0-6 values. 

If inittab or structured services are wanted for managing getty, etc, then the /etc/ttys file can be left empty and the /etc/rc file can just contain the launch of finit as finit services.

It is assumed that unless only told to go to 0 or 1 by the kernel, the rc init component will default to level 2.


Finitctl Quick controls:
------------------------

`finitctl stop` = stop all services \
`finitctl start` = start all services \
`finitctl kill` = stop all services and kill service system completely (force kill) \
`finitctl start blah` = start the service called blah if not already running \
`finitctl stop blah` = stop the service called blah if not already running \
`finitctl update` = rescan the services for changes/additions/deletions, change running services to match differences \
`finitctl level n` = change the service runlevel to `n` where `n` is a value between 0 and 6, inclusive. See info below about runlevels and meanings  


Structured Services with /etc/finit.d
-------------------------------------

/etc/finit.d can contain two types of service.

If they end in .sh or have the "#!" ident as the first two characters of the file they are simple shell scripts and a template service is created.
Simple script template services are executed as bootwait action, priority 20 (default for linux), and on service runlevels 3,4,5, with the command set to just the shell script to be run.
They may still be started/stopped if required.

Other files are parsed as finit services, which are an expanded form of the inittab format used by SysV style inits. Files beginning with a '.' are assumed hidden and will not be run. A file with the basename inittab is loaded without the extensions.

See https://www.ibm.com/docs/en/aix/7.1?topic=files-inittab-file for the base inittab service structure and action descriptions.

Each service entry that can be run, is run in concurrently, except for those marked as "wait" or where they must wait for dependencies.

The expanded entry form is thus:

`enable:identifier:priority:servicelevel:action:depends:console:command:`

The whole string is called the 'entry', ':' delimited strings are called 'components', and ';' delimited parts of components are called 'arguments'.

Identifier may be referred to as the entry name and is the name to be used when controlling the service with enable/disable/start/stop commands.

### Defaults and Limits of components:

If a component is blank then it is filled with a default. A shell script is given the defaults.

All strings are case-sensitive.

#### enable:

Defaults to disabled if blank, enabled if invalid argument.

Can be up to 512 characters, but only the first is processed.

#### name:

No name causes a critical error. ALL services MUST have a name.

If an .sh script, then the name is filled in with the script file's base name.

A name (and thus script filenames) MUST NOT contain spaces. If they do, then dependencies may fail to work correctly.

#### priority:

Defaults to 20, same as linux default

Can be up to 512 characters, but only decimal values 0 to 40 (where 40 is lowest priority) are valid. This is the Linux kernel priority and can be converted to/from 'nice' by: Nice = Prio - 20

#### service level:

Defaults to levels 3, 4, 5

Level 0 is for turning off the service controller, Level 1 - 5 are user defined by scripts, and Level 6 is for restarting the service controller. 

This must be a list of ASCII digits, e.g. "345". non-digit characters are ignored so comma, space, or semi-colon delims are allowed.

#### action:

Defaults to boot and wait

We have defined:

  1. boot: Run once during first launch of services only
  2. wait: Wait after running for the script/service to return
  3. once: Run once at boot, plus when changing to a valid level if not already running
  4. off: run when all services are stopped (regardless of runlevel)
  5. respawn: restart the service if it exits
  6. updater: see 2.
  7. onupdate: run or restart this service whenever an 'updater' action service returns
  8. ondemand: run only when manually started using finitctl; other options such as once, boot, and respawn override this

The action component may contain any combination, however defining respawn with other actions is undefined behaviour and may break the system, except for when used with an updater.

All services are killed if going from a level that allows to a level that doesn't.

The off action implies wait.


#### depends:

Defaults to no dependents.

Dependents may be any process name, delimited with ';' or ' ' characters. If a dependent does not exist, then it is ignored.

Depending on a disabled service is valid, but may cause unwanted behaviour. The system WILL NOT hang waiting for disabled processes, but the disable is functionally inherited until cleared.

Dependents are only processed when starting/stopping a service manually or when run as once or boot. 

#### console:

The file to use for stdio on this service. This defaults to inheriting the controlling tty from the init program itself.

Should only be used with proper /dev/tty files, if you want to route a service output to a text file, then use the '>>' or '>' in your command.

#### command:

No command causes a critical error. ALL services MUST have a command.

### Whitespace

White space is mostly ignored, blank lines are ignored, and comments may be entered around components or entries using # at the start of the line.

#### ...around arguments

Semi-colons may be used to separate terms inside a component to offer more arguments or information, however each argument of a service component MUST be on the same line. 

i.e. `makedev;udev:` is okay, but
```
makedev;
udev:
```
isn't.

#### ...around components

However the white space between the ':' of the last entry component and the first token character of the next component can be preceded with new lines or white space, and these are ignored. 

e.g. `y:     name:` is the same as `y:name:` is the same as 

```
y:

name:
```

Note, however, that the ':' for a component MUST be on the same line as the rest of the component.

An empty entry component MUST still have the ':', and is assumed to be the default.

#### ...around entries

Multiple entries in a file ignore whitespace just like around components. However, remember that the you must have the final ':' after the command component before starting the next entry.

### Expanded components

If the "enable" entry is blank, 'n' or 'N' then the service is added to the list, but disabled. Any other value is enabled. IBM SysV init uses ':' to comment out a service but as we may have multiple lines per entry this is ignored.

The "depends" entry allows you to specify another service that this one should wait for before continuing.
If a dependent service does not exist, then it is ignored. If a dependent service is disabled, then the depender service is not run.

The "priority" entry allows you to specify at what system priority a service is to start at. 20 is the default on Linux.

### e.g. independent service file:

```
#####################################################
# Simple Service for doing fstab automounts at boot #
#####################################################

# To disable manually, change to 'n'
y:

# name
automount:

# priority (default=20)
20:

# service levels
345:

# actions
boot:

# depends
makedev;udev:

# Console/TTY to use as controlling tty
:

# command
mount -a:
```

Multiple entries can be included in a single service file if required, so that the sysv inittab style of one-per-line may also be used if there are several services.

e.g.
```
y:automount:345:boot:::mount -a:
y:autoupgrade:345:respawn:automount::sleep 5000 && /media/usb/upgrade.sh:
```

You can mix and match multiline and single line in the same file or service entry.

Finit Structured Service runlevels
----------------------------------

0 = Off, turn off all services and stay at runlevel 0 until set to another.

1-5 = custom definitions

6 = Restart: stop all running services and restart them, return to previous runlevel.

As finit is independent of a runlevel init system and may be used with inits that operate in rc mode or without runlevels, the runlevel component of finit services is only used to specify service order or to allow launching some services without going further into others during boot.

The default runlevel when booting is 3.

Passing signals and changing runlevels:
---------------------------------------

### RC Init

When acting as rc init the following signals are applied

SIGKILL = issue SIGKILL to all processes, then HALT processor if possible. \
SIGTERM = issue SIGTERM to all processes, wait 5 seconds, then issue SIGKILL to all processes,  HALT processor if possible, if not just exit. \
SIGPWR  = as SIGTERM

### finit Services

When acting as finit services, the fifo file /run/finitctl is used to receive changes/information, the service runlevels are done by monitoring the fifo for ascii encoded 0-6 values. 

FIFO values/strings are delimited with '\n\0's

Changes are implemented as soon as the first one is seen.

The following signals are passed to finit services to denote changes:

SIGHUP  = Rescan services in /etc/finit.d for changes (may cause override warnings), and change runlevel if different. \
SIGUSR1 = Close and reopen the fifo file \
SIGUSR2 = Close the control fifo and and do not reopen it (may be used for power off) \
SIGINT = start services listed in finitctl by name, if none given then start all not already running for current runlevel \
SIGQUIT = quit service launcher/controller without handling service exits (for debugging in terminal) \
SIGTERM = stop services (issues sigterm) listed in finitctl by name, if none given then stop all \
SIGPWR  = As SIGTERM \
SIGKILL = issue SIGKILL to all processes, then HALT processor if possible (not usually catchable) \

Actions marked as off or shutdown will be run on SIGTERM, SIGPWR, and SIGUSR 1 or 2 when acting on all services.

The service name or runlevel to change to should be loaded into the FIFO before sending the signal
