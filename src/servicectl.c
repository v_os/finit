/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

finit services parts of finit controller

*/

#include "init.h"

#include <fcntl.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/**
 * Execute a command and get the result of stdout and optionally stderr
 *
 * @param   cmd - The system command to run.
 * @param   incl_STDERR - Whether to include the stderr stream as well as stdout
 * @param   len - size of the buffer to use when loading data, NOT the length to capture
 * @return  The string command line output of the command, or an empty (but not NULL) string if fail. You will need to free() the char* pointer after use.
 */
char* system_o(const char* cmd, int incl_STDERR, int len)
{
    static char data[512];  // allocate some space for it
    memset(data, 0, 512);

    // take a copy of the command string for any manipulation
    char* commedit = (char*)malloc((strlen(cmd) + 1 + strlen(" 2>&1")) * sizeof(char));
    strcpy(commedit, cmd);

    FILE* fstream;
    char buffer[len];

    // append the redirect from stderr to stdout if we need that
    if (incl_STDERR)
        commedit = strcat(commedit, " 2>&1");

    // clear anything we had left in the stdout buffer from the parent program
    fflush(stdout);

    // pipe+open (i.e. connect to the stdout to grab info when we run it)
    fstream = popen(commedit, "r");

    // if steam opened, then we can capture the data
    if (fstream)
    {
        // while data is still being written, capture it.
        while (!feof(fstream))
        {
            if (fgets(buffer, len, fstream) != NULL)  // if we can read this
            {
                strcat(data, buffer);
            }
        }
        fflush(fstream);
        pclose(fstream);  // Sometimes this corrupts data... why?
    }

    return data;
}

/* service controller */
int servicectl(int argc, char** argv, int as_sysv)
{
    int ret = 0;

    /* get the name */
    fprintf(stderr, _INIT_NAME "ctl: info: " _INIT_NAME " is pid ");

    char* pid_str = system_o(
      ("pidof " _INIT_NAME
       " 2>/dev/null | awk '{print $NF}'"),
      1,
      256);

    int pid = atol(pid_str);

    if (!pid)
    {
        fprintf(stderr, "-error: not running.\n");
        exit(1);
        return 1;
    }

    printf("%s", pid_str);

    // uid_t uid = getuid(), euid = geteuid();
    // if (uid > 0 || euid > 0)
    // {
    //     printf("You need root to control "_INIT_NAME
    //            "\n");
    //     exit(1);
    // }

    FILE* fd = fopen(_SERVICE_FIFO, "w");
    if (!fd)
    {
        fprintf(stderr, _INIT_NAME "ctl: crit: unable to open fifo control\n");
        exit(1);
        return 1;
    }

    argv++;
    argc--;

    int opt = 0;

#define CTL_OPT_START   0x01
#define CTL_OPT_STOP    0x02
#define CTL_OPT_RESTART 0x03
#define CTL_OPT_KILL    0x04
#define CTL_OPT_LEVEL   0x05
#define CTL_OPT_UPDATE  0x06

    if (strcmp(argv[0], "start") == 0)
    {
        opt = CTL_OPT_START;
    }
    else if (strcmp(argv[0], "restart") == 0)
    {
        opt = CTL_OPT_RESTART;
    }
    else if (strcmp(argv[0], "stop") == 0)
    {
        opt = CTL_OPT_STOP;
    }
    else if (strcmp(argv[0], "kill") == 0)
    {
        opt = CTL_OPT_KILL;
    }
    else if (strcmp(argv[0], "runlevel") == 0)
    {
        opt = CTL_OPT_LEVEL;
    }
    else if (strcmp(argv[0], "update") == 0)
    {
        opt = CTL_OPT_UPDATE;
    }

    argv++;
    argc--;

    /* copy service name (if given) into fifoctl */
    if (argc)
    {
        fprintf(fd, "%s\n", argv[0]);
        fflush(fd);
        // sleep(1);
    }

    switch (opt)
    {
    /* Update the service list */
    case CTL_OPT_UPDATE:
        {
            if (argc)
            {
                fprintf(stderr, _INIT_NAME "ctl: info: setting runlevel to \"%s\"\n", argv[0]);
            }
            fprintf(stderr, _INIT_NAME "ctl: info: updating service list\n");
            ret |= kill(pid, SIGHUP);
            break;
        }
    /* set the service runlevel */
    case CTL_OPT_LEVEL:
        {
            if (argc)
            {
                fprintf(stderr, _INIT_NAME "ctl: info: setting runlevel to \"%s\"\n", argv[0]);
            }
            else
            {
                fprintf(stderr, _INIT_NAME "ctl: info: setting runlevel to 3\n");
                fprintf(fd, "3\n");
                fflush(fd);
            }
            ret |= kill(pid, SIGHUP);
            break;
        }
    /* start a service or all services */
    case CTL_OPT_START:
        {
            if (argc)
            {
                fprintf(stderr, _INIT_NAME "ctl: info: starting service \"%s\"\n", argv[0]);
            }
            else
            {
                fprintf(stderr, _INIT_NAME "ctl: info: starting all services\n");
            }
            ret |= kill(pid, SIGINT);
            break;
        }
    /* stop then start a service (restart) */
    case CTL_OPT_RESTART:
        {
            if (argc)
            {
                fprintf(stderr, _INIT_NAME "ctl: info: restarting service \"%s\"\n", argv[0]);
            }
            else
            {
                fprintf(stderr, _INIT_NAME "ctl: info: restarting all services\n");
            }

            /* do the stop */
            ret |= kill(pid, SIGTERM);

            sleep(5);

            /* then just start it again */
            if (argc)
            {
                fprintf(fd, "%s\n", argv[0]);
                fflush(fd);
            }

            ret |= kill(pid, SIGINT);
            break;
        }
    /* stop a service or all services */
    case CTL_OPT_STOP:
        {
            if (argc)
            {
                fprintf(stderr, _INIT_NAME "ctl: info: stopping service \"%s\"\n", argv[0]);
            }
            else
            {
                fprintf(stderr, _INIT_NAME "ctl: info: stopping all services\n");
            }
            ret |= kill(pid, SIGTERM);
            break;
        }
    /* kill the init program. if PID 1, then this will do a clean shutdown */
    case CTL_OPT_KILL:
        {
            // if (argc)
            // {
            //     fprintf(stderr, _INIT_NAME "ctl: info: killing service \"%s\"\n", argv[0]);
            //     kill(pid, SIGTERM);
            //     sleep(5);
            //     fprintf(fd, "%s\n", argv[0]);
            //     fflush(fd);
            //     kill(pid, SIGKILL);
            // }
            // else
            // {
            fprintf(stderr, _INIT_NAME "ctl: info: killing "_INIT_NAME
                                       "\n");
            ret |= kill(pid, SIGTERM);
            sleep(1);
            ret |= kill(pid, SIGTERM);
            sleep(5);
            ret |= kill(-pid, SIGKILL);
            ret |= killpg(-pid, SIGKILL);
            //}
            break;
        }
    default:
        fprintf(stderr, _INIT_NAME "ctl: error: bad option %s\n", argv[0]);
        fclose(fd);
        exit(1);
        break;
    }

    fclose(fd);

    if (ret)
    {
        perror(_INIT_NAME "ctl: error");
    }

    return ret;
}
