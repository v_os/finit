/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "common.h"

#include "init.h"

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <limits.h>
#include <pwd.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __linux__
#include <sys/prctl.h>
#endif
#include <sys/ioctl.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

/* make a tty play nice (set attributes) */
int make_tty_nice()
{
    struct termios conopt;

    if (tcgetattr(0, &conopt) != 0)
        return 0;

    conopt.c_cc[VINTR] = 3;    /* C-c */
    conopt.c_cc[VQUIT] = 28;   /* C-\ */
    conopt.c_cc[VERASE] = 127; /* C-? */
    conopt.c_cc[VKILL] = 21;   /* C-u */
    conopt.c_cc[VEOF] = 4;     /* C-d */
    conopt.c_cc[VSTART] = 17;  /* C-q */
    conopt.c_cc[VSTOP] = 19;   /* C-s */
    conopt.c_cc[VSUSP] = 26;   /* C-z */

#if defined(__linux__) && __linux__
    conopt.c_line = 0;
#endif

    conopt.c_cflag &= CSIZE | CSTOPB | PARENB | PARODD | CRTSCTS;
    conopt.c_cflag |= CREAD | HUPCL | CLOCAL;

    conopt.c_lflag = ISIG | ICANON | ECHO | ECHOE | ECHOK | ECHOCTL | ECHOKE | IEXTEN;
    conopt.c_oflag = OPOST | ONLCR;
    conopt.c_iflag = ICRNL | IXON | IXOFF;

    tcsetattr(0, TCSANOW, &conopt);

    return 1;
}

/* set the controlling tty for a process */
int set_tty_device(char* console)
{
    if (console[0])
    {
        int tty;

        if (fcntl(0, F_GETFD) != -1)
            close(0);
        if (fcntl(1, F_GETFD) != -1)
            close(1);
        if (fcntl(2, F_GETFD) != -1)
            close(2);

        tty = open(console, O_RDWR | O_NOCTTY, 0644);

        if (tty < 0)
        {
            /* we failed to open, so try to set it to the default (/dev/console) */
            return set_console();
        }

        ioctl(0, TIOCSCTTY, 1);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        dup(tty);
        dup(tty);
#pragma GCC diagnostic pop

        make_tty_nice();
    }

    return 1;
}

/* set init to control /dev/console instead of whatever previous tty called it */
int set_console()
{
    char* con;

    /* try to grab from environment first */
    con = getenv("CONSOLE");
    if (!con)
        con = getenv("console");

    /* give up and grab it from the define */
    if (!con)
        con = (char*)_DEFAULT_CONSOLE;

    if (con && con[0])
    {
        int tty;

        if (fcntl(0, F_GETFD) != -1)
            close(0);
        if (fcntl(1, F_GETFD) != -1)
            close(1);
        if (fcntl(2, F_GETFD) != -1)
            close(2);

        tty = open(con, O_RDWR | O_NONBLOCK | O_NOCTTY, 0644);

        if (tty < 0)
        {
            fprintf(stderr, _INIT_NAME ": Crit: Unable to set main console\n");
            exit(1);
        }

        ioctl(0, TIOCSCTTY, 1);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        dup(tty);
        dup(tty);
#pragma GCC diagnostic pop

        make_tty_nice();
    }

    return 1;
}

/* find all files in a directory and call the passed func with the file path as argument */
int proc_directory(char* path, dir_func func)
{
    int r = 0;

    char* dpath = NULL;
    char env_buf[PATH_MAX];
    strncpy(env_buf, path, PATH_MAX);
    dpath = strtok(env_buf, delims);
    while (dpath != NULL)
    {
        DIR* dp;
        struct dirent* ep;
        dp = opendir(dpath);

        if (dp != NULL)
        {
            while ((ep = readdir(dp)) != NULL)
            {
                if (ep->d_name[0] != '.')
                {
                    char* fpath = (char*)malloc(strlen(dpath) + strlen(ep->d_name) + 1);
                    fpath[0] = 0;
                    strcpy(fpath, dpath);
                    if (fpath[strlen(fpath) - 1] != '/')
                    {
                        strcat(fpath, "/");
                    }
                    strcat(fpath, ep->d_name);

                    struct stat sbuf;

                    /* if file exists */
                    if (stat(fpath, &sbuf) == 0)
                    {
                        /* and is a regular file */
                        if (S_ISREG(sbuf.st_mode))
                        {
                            r |= func(fpath);
                        }
                    }
                }
            }
            closedir(dp);
        }
        dpath = strtok(NULL, delims);
    }
    return r;
}
