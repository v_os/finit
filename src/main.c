/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Very lightweight init and service system based on old-school rc inits combined with
some more modern comforts and sysv inittab and runlevels.

https://en.wikipedia.org/wiki/Init

See init.h to define which types of init are processed.
*/

#include "common.h"
#include "init.h"
#include "rcinit.h"
#include "servicectl.h"
#include "serviceload.h"

#include <libgen.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#if __linux__
#include <sys/prctl.h>
#endif

/* main entry point */
int main(int argc, char** argv)
{
#if __linux__
    /* gcompat on alpine gives this the wrong name, so quickly fix it */
    prctl(PR_SET_NAME, (unsigned long)(basename(argv[0])), 0, 0, 0);
#endif

    /* regardless of the program launching, show version if asked */
    if (argc >= 2 && (strcmp(argv[1], "version") == 0 || strcmp(argv[1], "--version") == 0 || strcmp(argv[1], "-V") == 0))
    {
#if !defined(CONTROLONLY)
        fprintf(stderr, "\n%s, v%s\n", ProductInfo, BuildNumber);
#else
        fprintf(stderr, "\n%s Controller, v%s\n", ProductInfo, BuildNumber);
#endif
        fprintf(stderr, "Published: %s\n", BuildDate);
        fprintf(stderr, "%s\n", CopyrightInfo);
        fprintf(stderr, "\n%s\n", LicenseInfo);
        fprintf(stderr, "%s\n", FreeSoftwareDisclaimer);

#if !defined(CONTROLONLY)
        fprintf(stderr, "This finit is built for: ");
#if DO_SYSV
        fprintf(stderr, "sysv init");
#endif
#if DO_RC && DO_SYSV
        fprintf(stderr, ", ");
#endif
#if DO_RC
        fprintf(stderr, "rc init");
#endif
#if (DO_SERVICES && DO_RC) || (DO_SERVICES && DO_SYSV)
        fprintf(stderr, ", & ");
#endif
#if DO_SERVICES
        fprintf(stderr, "services");
#endif
        fprintf(stderr, "\n\n");
#endif
        return 0;
    }

#if !defined(CONTROLONLY)
/* if do RC init */
#if DO_RC
    /* RC Init replacement */
    if (strcmp(basename((char*)argv[0]), ("init")) == 0)
        return rcinitload(argc, argv);
#endif

/* if do sysv init */
#if DO_SYSV
    /* SysV Init replacement */
    if (strcmp(basename((char*)argv[0]), ("init")) == 0 && getpid() == 1)
        return serviceload(argc, argv, 1);

    /* Runlevel adjuster for SysV init */
    if (strcmp(basename((char*)argv[0]), ("init")) == 0 && getpid() != 1)
        return servicectl(argc, argv, 1);

    /* ditto */
    if (strcmp(basename((char*)argv[0]), ("telinit")) == 0)
        return servicectl(argc, argv, 1);

    /* Alternative name for finitctl if used as extended sysv init */
    if (strcmp(basename((char*)argv[0]), ("initctl")) == 0)
        return servicectl(argc, argv, 1);
#endif

/* if do finit extended services */
#if DO_SERVICES
    /* Service launcher only */
    if (strcmp(basename((char*)argv[0]), (_INIT_NAME)) == 0)
        return serviceload(argc, argv, 0);

    /* Service control only */
    if (strcmp(basename((char*)argv[0]), (_INIT_NAME "ctl")) == 0)
        return servicectl(argc, argv, 0);
#endif
#else
    /* Service control only */
    if (strcmp(basename((char*)argv[0]), (_INIT_NAME "ctl")) == 0)
        return servicectl(argc, argv, 0);
#endif

    return 1;
}
