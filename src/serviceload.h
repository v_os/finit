/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

finit services parts of finit controller

*/

#if !H_SERVICELOAD
#define H_SERVICELOAD 1

#define _BOOT_FLAG_DEFAULT 1 /* whether to enable the first run flag */

#define _RUNLEVEL_DEFAULT 3 /* default runlevel to go to on first boot */

#define _DO_INITTAB 0 /* try to do /etc/inittab even if not called as sysv init */

#define _INITTAB_FILE "/etc/inittab"

#define _SCRIPT_DIRECTORY "/etc/" _INIT_NAME ".d"

#define _SERVICE_LINE_SIZ 512 /* max length of a single line for a service */

#define _SERVICE_MAX_SIZ 100 /* how many services to cache */

#define _SERVICE_FPATH_SIZ   256                /* service file name under /etc/ssos.d/ */
#define _SERVICE_NAME_SIZ    _SERVICE_FPATH_SIZ /* max length of service name */
#define _SERVICE_COMMAND_SIZ _SERVICE_LINE_SIZ  /* service command length */
#define _SERVICE_DEPENDS_SIZ _SERVICE_MAX_SIZ   /* number of services that a service can depend on */

#define _ACTION_BOOT     0x01 /* run during first launch only */
#define _ACTION_WAIT     0x02 /* run, then wait for it to finish/return before continuing to other services */
#define _ACTION_RESPAWN  0x04 /* rerun the service if it ever returns/finishes; if already running then ignore */
#define _ACTION_ONCE     0x08 /* run once during any change to a valid run level; do not restart if already running */
#define _ACTION_OFF      0x10 /* run when all services are turned off */
#define _ACTION_ONUPDATE 0x20 /* run/restart whenever builtin update service finishes */
#define _ACTION_UPDATER  0x40 /* trigger update actions when this service returns */
#define _ACTION_ONDEMAND 0x80 /* trigger only when asked to start manually */

struct structured_service {
    /* info about loaded */
    int pid;                        /* pid this service is running as */
    char file[_SERVICE_FPATH_SIZ];  /* file that service was launched from */
    int dids[_SERVICE_DEPENDS_SIZ]; /* list of indexes to other services that this service should wait for before running the first time */
    int lvlmsk;                     /* run levels as bitmask */
    int actmsk;                     /* actions as bitmask */

    /* raw cached info */
#define _CUR_DISABLE 0
    int disable; /* service disabled flag */
#define _CUR_NAME 1
    char name[_SERVICE_NAME_SIZ]; /* service name */
#define _CUR_PRIO 2
    int priority; /* priority to run service at */
#define _CUR_LEVELS 3
    char levels[_SERVICE_NAME_SIZ]; /* service run levels mask */
#define _CUR_ACTIONS 4
    char actions[_SERVICE_NAME_SIZ]; /* when/how to run this service */
#define _CUR_DEPENDS 5
    char depends[_SERVICE_LINE_SIZ]; /* dependents as raw string */
#define _CUR_CONSOLE 6
    char console[_SERVICE_FPATH_SIZ]; /* process's controlling terminal */
#define _CUR_COMMAND 7
    char command[_SERVICE_COMMAND_SIZ]; /* command to run */
};

#define _DISABLE_NO          (0)
#define _DISABLE_YES         (1)
#define _DISABLE_INHERIT_YES (2)

static const struct structured_service default_serv = {
  .disable = _DISABLE_YES,
  .name = "unknown",
  .priority = 20,
  .levels = "345",
  .actions = "boot;wait",
};

static const struct structured_service shell_serv = {
  .disable = _DISABLE_NO,
  .name = "unknown.sh",
  .priority = 20,
  .levels = "345",
  .actions = "boot;wait",
};

#define FORK_FINISHED (-3) /* it's done */
#define FORK_NO_RUN   (-2) /* can't run this task */
#define FORK_RUNNABLE (-1) /* is going to try and launch this task */
#define FORK_CHILD    (0) /* we are in the child process */
/* any other value is process running */

void run_shutdown_services();

void stop_service(struct structured_service* serv);
void start_service(struct structured_service* serv);

void stop_all_services(int do_shutdown);
void start_all_services();

void print_service(struct structured_service* serv);

int calculate_dependencies();
int service_launchable(struct structured_service* serv);
int create_worksorder();
int launch_service(struct structured_service* serv);
int launch_worksorder();

int parse_service(char* fpath);

int del_service(struct structured_service* todel);
int add_service(struct structured_service* toadd);

/* service runner/launcher */
int serviceload(int argc, char** argv, int as_sysv);

#endif
