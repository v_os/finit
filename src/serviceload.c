/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

finit services parts of finit controller

*/

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#ifndef __linux__
#define __linux__
#endif
#endif

#include "serviceload.h"

#include "common.h"
#include "init.h"

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <libgen.h>
#include <limits.h>
#include <pthread.h>
#include <pwd.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __linux__
#include <sys/prctl.h>
#endif
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

volatile int service_runlevel = _RUNLEVEL_DEFAULT;

/* default service directory; can also be done as PATH with ':' separated folders */
char* script_dir = (char*)_SCRIPT_DIRECTORY;

struct structured_service service[_SERVICE_MAX_SIZ];
int service_worksorder[_SERVICE_MAX_SIZ]; /* working buffer for which round of services to start first */
int removed[_SERVICE_MAX_SIZ];            /* services to remove from the list */

volatile int is_boot = _BOOT_FLAG_DEFAULT;

volatile int is_sysv = 0;

volatile int ctl_fd = -1;

volatile int sig_flag = 0;

char serv[_SERVICE_NAME_SIZ]; /* buffer for loading out of fifo */

/* get runlevel or service name from fifo */
int get_from_fifoctl(char* serv)
{
    /* clear old */
    memset(serv, 0, _SERVICE_NAME_SIZ);

    /* read */
    int err = read(ctl_fd, serv, _SERVICE_NAME_SIZ);

    if (err <= 0)
        return -1;

    if (isdigit(serv[0]))
    {
        int nlevel = atoi(serv);

        serv[0] = 0; /* blank it out so that it's not read as a service name */

        /* if a valid range */
        if (nlevel >= 0 && nlevel <= 6)
        {
            return nlevel;
        }
    }

    return service_runlevel;
}

/* open the controlling fifo */
int open_fifoctl()
{
    if (is_sysv)
    {
        mkdir("/run", 0644);
        mkfifo(_SYSV_FIFO, 0644);
        ctl_fd = open(_SYSV_FIFO, O_RDONLY | O_NONBLOCK);
    }
    else
    {
        mkdir("/run", 0644);
        mkfifo(_SERVICE_FIFO, 0644);
        ctl_fd = open(_SERVICE_FIFO, O_RDONLY | O_NONBLOCK);
    }

    if (ctl_fd < 0)
    {
        fprintf(stderr, _INIT_NAME ": crit: unable to open control pipe (read)\n");
        exit(1);
    }
    return 0;
}

/* close the controlling fifo */
int close_fifoctl()
{
    fsync(ctl_fd);
    close(ctl_fd);
    ctl_fd = -1;
    return 0;
}

/* compare non-running vars of a service */
int servicecmp(struct structured_service* a, struct structured_service* b)
{
    if (a->disable == _DISABLE_NO && b->disable == _DISABLE_YES)
        return 1;
    if (a->disable == _DISABLE_YES && b->disable == _DISABLE_NO)
        return 1;
    if (strcmp(a->name, b->name) != 0)
        return 1;
    if (a->priority != b->priority)
        return 1;
    if (strcmp(a->levels, b->levels) != 0)
        return 1;
    if (strcmp(a->actions, b->actions) != 0)
        return 1;
    if (strcmp(a->depends, b->depends) != 0)
        return 1;
    if (strcmp(a->console, b->console) != 0)
        return 1;
    if (strcmp(a->command, b->command) != 0)
        return 1;

    return 0;
}

/* scan service files and add them to the list */
int scan_services()
{
    /* set flag to say all services need removing from the list */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        if (service[i].name[0])
        {
            removed[i]++;
        }
    }

    /* if inittab is enabled, then add the things from /etc/inittab */
    if (is_sysv || _DO_INITTAB)
    {
        /* if we're not doing custom directories */
        if (strcmp(script_dir, _SCRIPT_DIRECTORY) == 0)
        {
            parse_service(_INITTAB_FILE);
        }
    }

    /* load all services from files in directory given (only if not sysv replacement) */
    if (!is_sysv)
    {
        proc_directory(script_dir, parse_service);
    }

    /* delete all those flagged as removed */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        if (service[i].name[0])
        {
            if (removed[i])
            {
                stop_service(&service[i]);
                del_service(&service[i]);
            }
        }
    }

    return 0;
}

/* run all the services marked as shutdown services */
void run_shutdown_services()
{
    /* foreach service */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        if (service[i].name[0] > 0)
        {
            /* if a shutdown/off service */
            if (service[i].actmsk & _ACTION_OFF)
            {
                if (service[i].pid < FORK_CHILD)
                {
                    /* mark runnable */
                    service[i].pid = FORK_RUNNABLE;

                    /* and try to launch it */
                    launch_service(&service[i]);
                }
            }
        }
    }
}

/* stop a single service */
void stop_service(struct structured_service* serv)
{
    if (serv->name[0] > 0)
    {
        /* if in parent */
        if (serv->pid > FORK_CHILD)
        {
            fprintf(stderr, _INIT_NAME ": info: stopping service %s\n", serv->name);
            kill(-serv->pid, SIGTERM);
            killpg(-serv->pid, SIGTERM);
            sleep(1);
            kill(-serv->pid, SIGKILL);
            killpg(-serv->pid, SIGKILL);
            while (!waitpid(serv->pid, NULL, 0))
            {
                ;
            }
            serv->pid = FORK_FINISHED;
        }
    }
}

/* start a single service (alias to launch_service) */
void start_service(struct structured_service* serv)
{
    launch_service(serv);
}

/* stop all services */
void stop_all_services(int do_shutdown)
{
    //   fprintf(stderr, _INIT_NAME ": info: stopping all services\n");

    /* stop all */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        stop_service(&service[i]);
    }

    sync();

    /* run shutdown services if told to */
    if (do_shutdown)
        run_shutdown_services();

    sync();

    /* clear up any stragglers from shutdown services */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        if (service[i].name[0] > 0)
        {
            if (service[i].pid > FORK_CHILD)
            {
                stop_service(&service[i]);
            }
        }
    }
}

/* start all services, with dependencies */
void start_all_services()
{
    //    fprintf(stderr, _INIT_NAME ": info: starting all services\n");

    /* scan each service and set flags to say which it depends on */
    calculate_dependencies();

    int stage = 0;

    /* keep creating worksorders and launching them until we clear dependencies */
    while (create_worksorder())
    {
        stage++;

        /* if we can't clear the dependencies, then exit with error */
        if (stage >= _SERVICE_MAX_SIZ)
        {
            fprintf(stderr, _INIT_NAME ": warn: start all finished with unmet dependencies\n");
            break;
        }

        launch_worksorder();
    }

    /* always launch the last one */
    launch_worksorder();

    /* any services marked as done, can now be marked as "do" */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        /* clear old works order */
        service_worksorder[i] = 0;
    }
}

/* signal handler for controling services */
void sig_hand(int sig)
{
    /* before processing, grab the info */
    int rl = get_from_fifoctl(serv);
    int old_rl = service_runlevel;

    char d;
    while (read(ctl_fd, &d, 1) > 0)
    {
        ;
    }

    if (isprint(serv[0]))
    {
        /* remove newline symbols */
        char* eol = strrchr(serv, '\n');
        if (eol != NULL)
        {
            *eol = '\0';
        }
        eol = strrchr(serv, '\r');
        if (eol != NULL)
        {
            *eol = '\0';
        }
        fprintf(stderr, _INIT_NAME ": info: signal on service name: %s\n", serv);
    }
    else
    {
        serv[0] = 0;
        memset(serv, 0, _SERVICE_NAME_SIZ);
    }

    sig_flag = sig;

    switch (sig)
    {
    /* Rescan services for changes (may cause override warnings), and change runlevel if different. */
    case SIGHUP:
        {
            /* change runlevel if we've been asked to */
            if (rl >= 0 && rl != service_runlevel && rl >= 0 && rl <= 6)
            {
                service_runlevel = rl;
                fprintf(stderr, _INIT_NAME ": info: runlevel changed to %i\n", service_runlevel);
            }

            fprintf(stderr, _INIT_NAME ": info: scanning service files\n");

            /* rescan service files */
            scan_services();

            /* for ones that changed, stop them */
            for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
            {
                /* if this one is on order (means changed) */
                if (service_worksorder[i] != 0)
                {
                    /* stop it */
                    stop_service(&service[i]);
                }

                /* if a service is no longer allowed at this runlevel, stop it */
                if (!(service[i].lvlmsk & (1 << service_runlevel)))
                {
                    stop_service(&service[i]);
                }
            }

            /* if a standard runlevel */
            if (service_runlevel > 0 && service_runlevel < 6)
            {
                /* recalculate dependencies and launch services suitable for new runlevel */
                start_all_services();
            }
            /* if entering the "shutdown" runlevel */
            else if (service_runlevel == 0)
            {
                stop_all_services(1);
                is_boot = 1;
            }
            /* if entering the "restart" runlevel */
            else if (service_runlevel == 6)
            {
                /* shutdown */
                stop_all_services(1);
                is_boot = 1;
                /* put runlevel back */
                service_runlevel = old_rl;
                /* start all services again */
                start_all_services();
            }
        }
        break;
    /* start services listed in initctl by name, if none given then start all not already running for current runlevel */
    case SIGINT:
        {
            if (serv[0])
            {
                for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
                {
                    if (strcmp(service[i].name, serv) == 0)
                    {
                        if (service[i].pid < FORK_CHILD)
                            service[i].pid = FORK_RUNNABLE;
                        start_service(&service[i]);
                        break;
                    }
                }
            }
            else
            {
                start_all_services();
            }
        }
        break;
    /* stop services (issues sigterm) listed in initctl by name, if none given then stop all */
    case SIGTERM:
#ifdef SIGPWR
    case SIGPWR:
#endif
    case SIGKILL:
        {
            if (serv[0])
            {
                for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
                {
                    if (strcmp(service[i].name, serv) == 0)
                    {
                        stop_service(&service[i]);
                        break;
                    }
                }
            }
            else
            {
                stop_all_services(1);
                is_boot = 1;
            }
        }
        break;
    /* close and reopen fifo */
    case SIGUSR1:
        fprintf(stderr, _INIT_NAME ": info: resetting control fifo\n");
        close_fifoctl();
        open_fifoctl();
        break;
    /* close fifo and then stop services (basically a soft shutdown/sleep) */
    case SIGUSR2:
        fprintf(stderr, _INIT_NAME ": info: doing soft shutdown/sleep of services\n");
        close_fifoctl();
        stop_all_services(0);
        break;
    default:
        break;
    }

    memset(serv, 0, _SERVICE_NAME_SIZ);
}

void set_sig_dfl()
{
    signal(SIGHUP, SIG_DFL);  /* rescan & change runlevel */
    signal(SIGUSR1, SIG_DFL); /* close and reopen control fifo */
    signal(SIGUSR2, SIG_DFL); /* close contorl fifo (may be used for soft shutdown) */
    signal(SIGINT, SIG_DFL);  /* start services */
    signal(SIGTERM, SIG_DFL); /* stop services, aka shutdown/off */
#ifdef SIGPWR
    signal(SIGPWR, SIG_DFL); /* shutdown/off */
#endif
    signal(SIGKILL, SIG_DFL); /* kill */

    signal(SIGQUIT, SIG_DFL); /* quit init without handling services */
}

void set_sig_hand()
{
    signal(SIGHUP, sig_hand);  /* rescan & change runlevel */
    signal(SIGUSR1, sig_hand); /* close and reopen control fifo */
    signal(SIGUSR2, sig_hand); /* close control fifo (may be used for soft shutdown) */
    signal(SIGINT, sig_hand);  /* start services */
    signal(SIGTERM, sig_hand); /* stop services, aka shutdown/off */
#ifdef SIGPWR
    signal(SIGPWR, sig_hand); /* shutdown/off */
#endif
    signal(SIGKILL, sig_hand); /* kill */

    signal(SIGQUIT, SIG_DFL); /* quit init without handling services (keep default) */
}

/* print the current service */
void print_service(struct structured_service* serv)
{
    fprintf(stderr, "\nName \"%s\"\n", serv->name);
    fprintf(stderr, "From file %s\n", serv->file);
    fprintf(stderr, "PID %i\n", serv->pid);
    fprintf(stderr, "Enabled: %c\n", (serv->disable ? 'n' : 'y'));
    fprintf(stderr, "Priority: %i\n", serv->priority);
    fprintf(stderr, "Level Mask: %02x\n", serv->lvlmsk);
    fprintf(stderr, "Action Mask: %02x\n", serv->actmsk);
    fprintf(stderr, "Depends: %s\n", serv->depends);
    fprintf(stderr, "Stdio Term: %s\n", serv->console);
    fprintf(stderr, "Command: %s\n\n", serv->command);
}

/* delete a service from the list */
int del_service(struct structured_service* todel)
{
    /* try to find a free slot */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        /* if this one is not free, and the name matches */
        if (service[i].name[0] != 0 && strcmp(service[i].name, todel->name) == 0)
        {
            fprintf(stderr, _INIT_NAME ": info: Service \"%s\" from %s deleted from list\n", todel->name, todel->file);

            /* delete it from the array */
            memset(&service[i], 0, sizeof(struct structured_service));

            /* flag that it doesn't need removing now */
            removed[i] = 0;
        }
    }
    return 0;
}

/* add a service to the list */
int add_service(struct structured_service* toadd)
{
    /* try to find a free slot */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        /* if this one is free, or the name matches (same service) */
        if (service[i].name[0] == 0 || strcmp(service[i].name, toadd->name) == 0)
        {
            /* flag that it doesn't need removing */
            removed[i] = 0;

            /* if service already exists */
            if (strcmp(service[i].name, toadd->name) == 0)
            {
                /* compare for differences */
                if (servicecmp(&service[i], toadd))
                {
                    fprintf(stderr, _INIT_NAME ": warn: Service \"%s\" already exists, overriding\n", toadd->name);

                    /* flag that it changed */
                    service_worksorder[i]++;
                }
                else
                {
                    /* don't process any further otherwise*/
                    return 0;
                }
            }

            /* setup service runlevels mask */
            toadd->lvlmsk = 0;
            for (char* lvl = toadd->levels; *lvl != 0; lvl++)
            {
                if (isdigit(*lvl))
                {
                    toadd->lvlmsk |= (1 << (*lvl - '0'));
                }
            }

            /* setup actions mask */
            toadd->actmsk = 0;
            if (strstr(toadd->actions, "boot") != NULL)
            {
                toadd->actmsk |= _ACTION_BOOT;
            }
            if (strstr(toadd->actions, "sysinit") != NULL)
            {
                toadd->actmsk |= _ACTION_BOOT;
            }
            if (strstr(toadd->actions, "wait") != NULL)
            {
                toadd->actmsk |= _ACTION_WAIT;
            }
            if (strstr(toadd->actions, "onupdate") != NULL)
            {
                toadd->actmsk |= _ACTION_ONUPDATE;
            }
            if (strstr(toadd->actions, "updater") != NULL)
            {
                toadd->actmsk |= _ACTION_UPDATER;
            }
            if (strstr(toadd->actions, "ondemand") != NULL)
            {
                toadd->actmsk |= _ACTION_ONDEMAND;
            }
            if (strstr(toadd->actions, "respawn") != NULL)
            {
                toadd->actmsk |= _ACTION_RESPAWN;
            }
            if (strstr(toadd->actions, "restart") != NULL)
            {
                toadd->actmsk |= _ACTION_RESPAWN;
            }
            if (strstr(toadd->actions, "once") != NULL)
            {
                toadd->actmsk |= _ACTION_ONCE;
            }
            if (strstr(toadd->actions, "off") != NULL)
            {
                toadd->actmsk |= _ACTION_OFF;
            }
            if (strstr(toadd->actions, "shutdown") != NULL)
            {
                toadd->actmsk |= _ACTION_OFF;
            }

            /* if respawn and any other is defined, then warn that this is undefined behaviour */
            if (toadd->actmsk & _ACTION_RESPAWN && toadd->actmsk & ~_ACTION_RESPAWN)
            {
                fprintf(stderr, _INIT_NAME ": warn: Respawn behaviour when used with other actions may be unexpected\n");
            }

            /* Set to say a valid service, but unknown runstate */
            if (service[i].pid <= FORK_CHILD)
            {
                toadd->pid = FORK_NO_RUN;
            }
            /* otherwise preserve the old pid so we don't lose it */
            else
            {
                toadd->pid = service[i].pid;
            }

            /* if the name is a tty, and it's missing a term name set the process terminal to the name */
            if (strstr(toadd->name, "tty") != NULL && toadd->console[0] == 0)
            {
                if (strstr(toadd->name, "/dev/") == NULL)
                {
                    strcpy(toadd->console, "/dev/");
                }
                else
                {
                    toadd->console[0] = 0;
                }
                strcat(toadd->console, toadd->name);
            }

            // print_service(toadd);

            memcpy(&service[i], toadd, sizeof(struct structured_service));

            fprintf(stderr, _INIT_NAME ": info: Service \"%s\" from %s added to list\n", toadd->name, toadd->file);

            return 0;
        }
    }
    fprintf(stderr, _INIT_NAME "warn: Service list is full\n");
    return 1;
}

/* parse a file into services */
int parse_service(char* fpath)
{
    int skip_extensions = 0;

    /* if this is an inittab file, then don't do the extensions */
    if (strcmp(basename(fpath), "inittab") == 0)
    {
        skip_extensions++;
    }

    FILE* file;

    int fline = 1;
    char buf[_SERVICE_LINE_SIZ];
    char line[_SERVICE_LINE_SIZ];
    memset(buf, 0, _SERVICE_LINE_SIZ);
    memset(line, 0, _SERVICE_LINE_SIZ);

    /* create pointers for each part of the service entry */
    struct structured_service serv_to_add;
    memset((void*)&serv_to_add, 0, sizeof(struct structured_service));

    int cur = 0;

    /* skip the disable entry if not doing extensions */
    if (skip_extensions)
    {
        cur = _CUR_NAME;
        serv_to_add.priority = default_serv.priority;
    }

    serv_to_add.disable = 0;

    int added_ok = 0;

    file = fopen(fpath, "r");

    if (file == NULL)
    {
        perror(_INIT_NAME ": crit");
        exit(1);
    }
    else
    {
        int is_sh = 0;
        char* ext = strrchr(fpath, '.');
        if (ext != NULL)
        {
            is_sh = strcmp(++ext, "sh") == 0 || strcmp(ext, "SH") == 0;
        }

        /* get each line in turn */
        while (fgets(buf, _SERVICE_LINE_SIZ, file) != NULL)
        {
            /* if this file is an sh script (we do it here to ignore blank scripts) */
            if ((buf[0] == '#' && buf[1] == '!' && fline) || is_sh)
            {
                /* shell script, so set up a special file */
                serv_to_add.disable = shell_serv.disable;

                /* make the name the basename for the script */
                strncpy(serv_to_add.name, basename(fpath), _SERVICE_NAME_SIZ);

                /* save filepath */
                strncpy(serv_to_add.file, fpath, _SERVICE_FPATH_SIZ);

                /* set default priority, actions, and run levels */
                serv_to_add.priority = shell_serv.priority;

                strcpy(serv_to_add.actions, shell_serv.actions);

                strcpy(serv_to_add.levels, shell_serv.levels);

                /* set the command to just the fpath/script to execute */
                strcpy(serv_to_add.command, fpath);

                /* add service to the list */
                add_service(&serv_to_add);

                /* and then clear it (it's a security thing...) */
                memset((void*)&serv_to_add, 0, sizeof(struct structured_service));

                return 0;
            }
            fline = 0;

            char* current = NULL;

            /* Skip leading whitespaces */
            for (current = buf; *current == ' ' || *current == '\t'; (current)++)
            {
                ;
            }

            /* if this line is blank or a comment, skip to the next one */
            if (*current == '#' || *current == '\n' || strlen(current) < 1)
            {
                current = NULL;
                continue;
            }

            /* Trim the trailing \n and convert NULL */
            char* eol = strrchr(current, '\n');
            if (eol != NULL)
            {
                *eol = '\0';
            }

            /* buffer the line in case we need it for an error message */
            memcpy(line, buf, _SERVICE_LINE_SIZ);

            /* for each ':' split string */
            while (current != NULL && *current)
            {
                /* Skip leading whitespaces */
                for (; *current == ' ' || *current == '\t'; (current)++)
                {
                    ;
                }

                /* if this line is now empty */
                if (strlen(current) < 1)
                {
                    break;
                }

                char* splt = strchr(current, ':');
                if (splt != NULL)
                {
                    *splt = '\0';
                }

                switch (cur)
                {
                case _CUR_DISABLE:
                    if (*current == 'n' || *current == 'N' || *current == ' ' || *current == '\0')
                    {
                        serv_to_add.disable = _DISABLE_YES;
                    }
                    break;
                case _CUR_NAME:
                    strncpy(serv_to_add.name, current, _SERVICE_NAME_SIZ);
                    /* if skipping the extensions, then assume a blank name is disable, and retry */
                    if (skip_extensions && strlen(serv_to_add.name) < 1 && !serv_to_add.disable)
                    {
                        serv_to_add.disable = 1;
                        cur--;
                    }
                    else if (strlen(serv_to_add.name) < 1)
                    {
                        fprintf(stderr, _INIT_NAME ": crit: Service in \"%s\" is missing name\n", fpath);
                        exit(1);
                    }
                    break;
                case _CUR_PRIO:
                    serv_to_add.priority = atoi(current);
                    if (serv_to_add.priority == 0)
                        serv_to_add.priority = default_serv.priority;
                    break;
                case _CUR_LEVELS:
                    strncpy(serv_to_add.levels, current, _SERVICE_NAME_SIZ);
                    if (strlen(serv_to_add.levels) < 1)
                        strcpy(serv_to_add.levels, default_serv.levels);
                    break;
                case _CUR_ACTIONS:
                    strncpy(serv_to_add.actions, current, _SERVICE_NAME_SIZ);
                    if (strlen(serv_to_add.actions) < 1)
                        strcpy(serv_to_add.actions, default_serv.actions);
                    break;
                case _CUR_DEPENDS:
                    strncpy(serv_to_add.depends, current, _SERVICE_LINE_SIZ);
                    break;
                case _CUR_CONSOLE:
                    strncpy(serv_to_add.console, current, _SERVICE_FPATH_SIZ);
                    break;
                case _CUR_COMMAND:
                    strncpy(serv_to_add.command, current, _SERVICE_COMMAND_SIZ);
                    if (strlen(serv_to_add.command) < 1)
                    {
                        fprintf(stderr, _INIT_NAME ": crit: Service \"%s\" in \"%s\" is missing command\n", serv_to_add.name, fpath);
                        exit(1);
                    }
                    break;
                default:
                    break;
                }

                cur++;

                /* if not doing extensions, skip passed not-doable ones */
                if (skip_extensions)
                {
                    if (cur == _CUR_PRIO)
                        cur++;
                    if (cur == _CUR_DEPENDS)
                        cur++;
                    if (cur == _CUR_CONSOLE)
                        cur++;
                }

                if (cur > _CUR_COMMAND)
                {
                    /* copy fpath into service */
                    strncpy(serv_to_add.file, fpath, _SERVICE_FPATH_SIZ);

                    /* add service to the list */
                    added_ok |= add_service(&serv_to_add);

                    /* and then clear in case there's another one in this file */
                    memset((void*)&serv_to_add, 0, sizeof(struct structured_service));

                    /* reset back to current */
                    cur = 0;

                    if (skip_extensions)
                    {
                        cur = _CUR_NAME;
                        serv_to_add.priority = default_serv.priority;
                    }

                    /* and continue back to next line */
                    break;
                }

                if (splt != NULL)
                {
                    current = splt + 1;
                }
            }
        }
    }
    return !added_ok;
}

/* create a dependency list for each service */
int calculate_dependencies()
{
    /* do this twice just to make sure we catch stragglers. */
    for (int t = 0; t < 1; t++)
    {
        /* for each service */
        for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
        {
            /* if this one is not free */
            if (service[i].name[0] != 0)
            {
                /* for each service */
                for (int j = 0; j < _SERVICE_MAX_SIZ; j++)
                {
                    /* if this one is not free */
                    if (service[j].name[0] != 0)
                    {
                        /* if there are dependencies */
                        if (strlen(service[j].depends) >= 1)
                        {
                            char* dpend = NULL;

                            /* buffer depends for split */
                            char dbuf[_SERVICE_LINE_SIZ];
                            strncpy(dbuf, service[j].depends, _SERVICE_LINE_SIZ);

                            dpend = strtok(dbuf, argdelims);
                            while (dpend != NULL)
                            {
                                /* if one of the dependencies matches the name */
                                if (strcmp(service[i].name, dpend) == 0)
                                {
                                    /* if service depends on itself, critical error */
                                    if (strcmp(service[j].name, dpend) == 0)
                                    {
                                        fprintf(stderr, _INIT_NAME ": crit: Cyclical dependency detected\n");
                                        exit(1);
                                    }

                                    /* flag that this service needs to wait for the previous one to start */
                                    service[j].dids[i] |= 1;

                                    /* if the dependent has more run levels than the parent */
                                    if ((service[j].lvlmsk ^ service[i].lvlmsk) & service[j].lvlmsk)
                                    {
                                        fprintf(stderr, _INIT_NAME ": warn: \"%s\" depends on service \"%s\" with different runlevels\n", service[j].name, service[i].name);
                                    }

                                    if (service[i].disable == _DISABLE_YES)
                                    {
                                        fprintf(stderr, _INIT_NAME ": warn: \"%s\" depends on disabled service \"%s\"\n", service[j].name, service[i].name);
                                        if (service[j].disable == _DISABLE_NO)
                                            service[j].disable = _DISABLE_INHERIT_YES;
                                    }

                                    if (service[i].disable == _DISABLE_INHERIT_YES)
                                    {
                                        fprintf(stderr, _INIT_NAME ": warn: \"%s\" depends on disabled service (inherited) \"%s\"\n", service[j].name, service[i].name);
                                        if (service[j].disable == _DISABLE_NO)
                                            service[j].disable = _DISABLE_INHERIT_YES;
                                    }
                                }
                                else
                                {
                                    service[j].dids[i] = 0;
                                }

                                dpend = strtok(NULL, argdelims);
                            }
                        }
                        else
                        {
                            service[j].dids[i] = 0;
                        }
                    }
                }
            }
        }
    }

    return 0;
}

/* returns 1 if service may be launched (even if  it requires pid flags changing) */
int service_launchable(struct structured_service* serv)
{
    /* if it exists at all */
    if (serv->name[0] != 0)
    {
        /* if not disabled */
        if (serv->disable == _DISABLE_NO)
        {
            /* if it can be processed at this runlevel */
            if (serv->lvlmsk & (1 << service_runlevel))
            {
                /* if it isn't already running */
                if (serv->pid < FORK_CHILD)
                {
                    return 1;
                }
            }
        }
    }
    return 0;
}

/* create a worksorder from the current level and dependencies */
int create_worksorder()
{
    int retcode = 0;

    /* for each services */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        /* clear old works order */
        service_worksorder[i] = 0;

        /* if this service may be runnable */
        if (service_launchable(&service[i]))
        {
            /* create an order for it to be launched */
            service_worksorder[i] = 1;

            /* for each possible dependency */
            for (int d = 0; d < _SERVICE_MAX_SIZ; d++)
            {
                /* if this is a dependency */
                if (service[i].dids[d])
                {
                    /* if the dependency has not run and isn't running */
                    if (!(service[d].pid == FORK_FINISHED || service[d].pid > FORK_CHILD))
                    {
                        /* cancel from this works order as we're waiting on dependencies still */
                        service_worksorder[i] = 0;

                        /* return 1 to say we need to keep doing loop, hoping that dependencies clear up */
                        retcode = 1;

                        /* no need to check other dependencies */
                        break;
                    }
                }
            }
        }
    }

    /* this isn't useful when we have a lot of services... */
    // if (_SERVICE_MAX_SIZ <= 10)
    // {
    //     fprintf(stderr, "Created w/o: | ");
    //     for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    //     {
    //         fprintf(stderr, "%i: %c | ", i, (service_worksorder[i] ? 'y' : 'n'));
    //     }
    //     fprintf(stderr, "\n");
    // }

    return retcode;
}

/* launch a single service */
int launch_service(struct structured_service* serv)
{
    /* launchable */
    if (service_launchable(serv))
    {
        /* if it's boot-time only */
        if (serv->actmsk & _ACTION_BOOT)
        {
            serv->pid = FORK_NO_RUN;

            if (is_boot)
                if (serv->pid != FORK_FINISHED)
                    serv->pid = FORK_RUNNABLE;
        }

        /* if it's once only */
        if (serv->actmsk & _ACTION_ONCE)
        {
            serv->pid = FORK_NO_RUN;

            if (serv->pid != FORK_FINISHED)
                serv->pid = FORK_RUNNABLE;
        }

        /* if it's respawnable */
        if (serv->actmsk & _ACTION_RESPAWN)
        {
            serv->pid = FORK_NO_RUN;

            /* if not running, relaunch */
            if (serv->pid < FORK_CHILD)
                serv->pid = FORK_RUNNABLE;
        }

        /* create child on demand only */
        if (serv->actmsk & _ACTION_ONDEMAND)
        {
            /* if not already flaged as runnable, then don't run it */
            if (serv->pid != FORK_RUNNABLE)
                serv->pid = FORK_NO_RUN;
        }

        /* if can run this service */
        if (serv->pid == FORK_RUNNABLE)
        {
            fprintf(stderr, _INIT_NAME ": info: starting service %s\n", serv->name);
            /* fork to it */
            serv->pid = fork();

            if (serv->pid == FORK_CHILD)
            {
                /* set signals to default for child */
                set_sig_dfl();

#ifdef __linux__
                /* rename the child so that we don't accidentally send our signals to it */
                if (serv->name[0])
                    prctl(PR_SET_NAME, (unsigned long)&serv->name[0], 0, 0, 0);
                else
                    prctl(PR_SET_NAME, (unsigned long)&serv->command[0], 0, 0, 0);

                /* set priority as requested (nice = prio - 20) */
                if (serv->priority <= 40 && serv->priority > 0)
                {
                    setpriority(PRIO_PROCESS, 0, serv->priority - 20);
                }
                else if (serv->priority == 0)
                {
                    setpriority(PRIO_PROCESS, 0, 0);
                }
                else if (serv->priority > 40)
                {
                    struct sched_param s;
                    s.sched_priority = serv->priority;
                    pthread_setschedparam(pthread_self(), PTHREAD_INHERIT_SCHED, &s);
                }

#endif

                setsid();

                set_tty_device(serv->console);

                int ret = system(serv->command);
                exit(ret);
            }

            /* if we're supposed to wait after fork, then wait */
            if (serv->actmsk & _ACTION_WAIT || serv->actmsk & _ACTION_OFF)
            {
                while (!waitpid(serv->pid, NULL, 0))
                {
                    ;
                }

                serv->pid = FORK_FINISHED;
            }
        }
    }
    else
    {
        serv->pid = FORK_NO_RUN;
    }
    return 0;
}

/* launch services in the worksorder */
int launch_worksorder()
{
    /* foreach service */
    for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
    {
        /* if this one is on order */
        if (service_worksorder[i] != 0)
        {
            /* try to launch it */
            launch_service(&service[i]);
        }
    }
    return 0;
}

/* infinite loop that waits for services to end and then handles what to do if they do */
int service_waitloop()
{
    /* wait for one of the children to die */
    int retcode = -1;
    pid_t kid = wait(&retcode);

    /* if one died */
    if (kid > 1)
    {
        /* try and find a matching service */
        for (int i = 0; i < _SERVICE_MAX_SIZ; i++)
        {
            if (service[i].name[0])
            {
                /* if pids match */
                if (service[i].pid == kid)
                {
                    /* flag as finished */
                    service[i].pid = FORK_FINISHED;

                    /* is an updater then see if there are any "onupdate" ones to launch */
                    if (service[i].actmsk & _ACTION_UPDATER)
                    {
                        /* and returned with success */
                        if (WIFEXITED(retcode) && WEXITSTATUS(retcode) == EXIT_SUCCESS)
                        {
                            /* for each service */
                            for (int d = 0; d < _SERVICE_MAX_SIZ; d++)
                            {
                                /* if there's one that depended on this updater and is supposed to be triggered on update */
                                if (service[d].dids[i] && service[d].actmsk & _ACTION_ONUPDATE)
                                {
                                    /* and it's not already running */
                                    if (service[d].pid < FORK_CHILD)
                                    {
                                        /* mark runnable */
                                        service[d].pid = FORK_RUNNABLE;

                                        /* and try to launch it */
                                        launch_service(&service[d]);
                                    }
                                }
                            }
                        }
                    }

                    /* if it's a respawner then we can launch it again */
                    if (service[i].actmsk & _ACTION_RESPAWN)
                    {
                        /* note that we've assumed that dependencies are still met! */
                        launch_service(&service[i]);
                    }

                    /* break so that we don't process other services */
                    break;
                }
            }

            if (i == _SERVICE_MAX_SIZ - 1)
            {
                // fprintf(stderr, _INIT_NAME ": warn: Unknown child pid received by wait\n");
            }
        }
    }

    sched_yield();
    sleep(1);
    return 1;
}

/* service runner/launcher */
int serviceload(int argc, char** argv, int as_sysv)
{
    /* use args to know which directory to load scripts from */
    for (argc--; argc > 0; argc--)
    {
        argv++;

        struct stat sbuf;
        /* if file exists */
        if (stat(argv[0], &sbuf) == 0)
        {
            /* and is a directory */
            if (S_ISDIR(sbuf.st_mode))
            {
                /* then use this one instead of default */
                script_dir = argv[0];
            }
        }
    }

    /* try to take control of the console device */
#if !_PRINT_TO_TTY
    set_console();
#endif

    /* if this *IS* init or was the first thing called by init, then we need to set a program group and run the console */
    if (getpid() < 3)
    {
        setsid();
        set_console();
    }

    is_sysv = as_sysv;

    open_fifoctl();

    scan_services();

    start_all_services();

    /* we've finished the first boot launch, so clear flag */
    is_boot = 0;

    /* wait to catch straggling services */
    sleep(1);

    /*  set the signal handler function onto the control signals */
    set_sig_hand();

    /* and then loop to check for finished children */
    while (service_waitloop())
        ;

    return 0;
}
