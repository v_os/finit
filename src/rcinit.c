/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

True sysv init parts of finit controller

*/

#include "rcinit.h"

#include "init.h"

#include <fcntl.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

/* not yet implemented */
int rcinitload(int argc, char** argv)
{
    sigset_t set;

    if (getpid() != 1)
        return 1;

    sigfillset(&set);
    sigprocmask(SIG_BLOCK, &set, 0);
    switch (fork())
    {
        /* In child: do the rc init */
    case 0:
        {
            sigprocmask(SIG_UNBLOCK, &set, 0);
            setsid();
            setpgid(0, 0);
            return execve("/etc/rc", (char*[]) {"rc", 0}, (char*[]) {0});
            break;
        }
        /* error */
    case -1:
        {
            perror("fork");
            break;
        }
        /* in parent, just wait for child */
    default:
        {
            int status;
            while (1)
            {
                wait(&status);
            }
            break;
        }
    }
    return 1;
}
