/*
Copyright 2022 Chloe Lunn <chloetlunn@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Very lightweight init and service system based on old-school rc inits combined with
some more modern comforts and sysv inittab and runlevels.

https://en.wikipedia.org/wiki/Init

Use the DO_... defines below to allow/disallow types of init system

*/

#if !H_INIT
#define H_INIT 1

#define _PRINT_TO_TTY 1 /* disable rerouting stdio to /dev/console */

#ifndef DO_SYSV
#define DO_SYSV 1 /* Use finit services as a sysv init replacement */
#endif

#ifndef DO_RC
#define DO_RC 1 /* do rc style inits */
#endif          /* despite the names, this is implemented as part of sysvinit.c */

#ifndef DO_SERVICES
#define DO_SERVICES 1 /* do services */
#endif

#define _INIT_NAME "finit"

#define _DEFAULT_CONSOLE "/dev/console"
#define _ENTRY_DELIMS    ":"
#define _ARG_DELIMS      "; "

#define _SERVICE_FIFO ("/run/"_INIT_NAME \
                       "ctl")

#define _SYSV_FIFO ("/run/initctl")

static const char delims[] = _ENTRY_DELIMS;
static const char argdelims[] = _ARG_DELIMS;

static const char* ProductInfo = "Figbox Init & Services";
static const char* CopyrightInfo = "Copyright (c) 2022 Chloe Lunn, All rights reserved";
static const char* BuildDate = "2022-07-14";
static const char* BuildNumber = "1.0.0";
static const char* LicenseInfo = "License MIT: <https://opensource.org/licenses/MIT>";
static const char* FreeSoftwareDisclaimer = "This is free software; you are free to change and redistribute it.\r\n"
                                            "There is NO WARRANTY, to the extent permitted by law.\r\n";

#endif
